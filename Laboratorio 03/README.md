## Observaciones y Conclusiones
 1. El CRM (Customer Relationship Management) se refiere a la estrategia de negocios que la empresa utiliza para conseguir la fidelización del cliente. 
2. El CRM en Odoo puede gestionar la cartera de clientes, tanto potenciales como existentes, sencilla y eficazmente, siendo esta información accesible para todo el personal autorizado de la compañía. 
3. El módulo CRM no sólo está limitado a la relación con los clientes, si no que se refiere a todas aquellas personas o entidades relacionadas con la organización: proveedores, socios, empleados, etc.
4. Para la edición de la página web se puede arrastrar y soltar cualquier oportunidad de negocio de una etapa a otra en tan sólo unos pocos clics.
5. El CRM de Odoo es una herramienta de gestión de ventas que es realmente eficiente, el equipo de trabajo como el gerente pueden estar bien organizados. El gerente puede tener una visión mas clara del flujo de ventas, las reuniones, los ingresos, y demás.
6. Odoo permite la integración con el correo electrónico, existiendo la posibilidad de organizar y filtrar automáticamente los correos entrantes enfocados hacia las oportunidades o equipos de ventas.
---
Se adjunta excel
---