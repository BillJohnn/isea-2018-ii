## Laboratorio 9
## CONFIGURACI�N DE MODULOS
1. Detener el servicio de Odoo e iniciar en modo consola.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/41cddb625599d63d3aa9114fc0394244e7fbc551/Laboratorio%2009/Imagenes/Detener%20el%20servicio%20de%20Odoo%20e%20iniciar%20en%20modo%20consola.gif)]

2. Agregar una carpeta para nuestros m�dulos de terceros.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/41cddb625599d63d3aa9114fc0394244e7fbc551/Laboratorio%2009/Imagenes/Agregar%20una%20carpeta%20para%20nuestros%20m%C3%B3dulos%20de%20terceros.gif)]

3. Descargar e instalar el m�dulo odoo_web_login.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/41cddb625599d63d3aa9114fc0394244e7fbc551/Laboratorio%2009/Imagenes/Descargar%20e%20instalar%20el%20m%C3%B3dulo%20odoo_web_login.gif)]

4. Descargar e instalar un tema de Odoo, de preferencia distinto al mostrado en la lista de reproducci�n.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/41cddb625599d63d3aa9114fc0394244e7fbc551/Laboratorio%2009/Imagenes/Descargar%20e%20instalar%20un%20tema%20de%20Odoo%2C%20de%20preferencia%20distinto%20al%20mostrado%20en%20la%20lista%20de%20reproducci%C3%B3n..gif)]

5. Buscar e instalar un m�dulo que permita descargar un archivo Excel de nuestro stock actual.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/41cddb625599d63d3aa9114fc0394244e7fbc551/Laboratorio%2009/Imagenes/Buscar%20e%20instalar%20un%20m%C3%B3dulo%20que%20permita%20descargar%20un%20archivo%20Excel%20de%20nuestro%20stock%20actual.gif)]

6. Buscar un m�dulo que me permita agregar los ubigeos de Per� (regiones, provincias, distritos).
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/src/master/Laboratorio%2009/Imagenes/Buscar%20un%20m%C3%B3dulo%20que%20me%20permita%20agregar%20los%20ubigeos%20de%20Per%C3%BA%20(regiones%2C%20provincias%2C%20distritos).gif)]


###### Observaciones y Conclusiones
1. Los m�dulos son bloques para la construcci�n de las aplicaciones en Odoo. Un m�dulo puede agregar o modificar caracter�sticas en Odoo.
2. Los m�dulos son soportados por un directorio que contiene un archivo de manifiesto o descriptor (llamado __openerp__.py) y el resto de los archivos que implementan sus caracter�sticas. 
3. Las aplicaciones no son diferentes de los m�dulos �stas proporcionan una caracter�stica central, alrededor de la cual otros m�dulos agregan caracter�sticas u opciones. Estas proveen los elementos base para un �rea funcional, como contabilidad o RRHH, sobre las cuales otros m�dulos agregan caracter�sticas. 
4. Lo m�s frecuente ser�n situaciones donde las modificaciones y extensiones son necesarias en un m�dulo existente para ajustarlo a casos de uso espec�ficos.
5. Debemos asegurarnos que el directorio que contiene el m�dulo sea parte de la ruta de complementos addons. Y luego tenemos que actualizar la lista de m�dulos de Odoo.
6. Al agregar campos en el modelo, que es necesaria una actualizaci�n del m�dulo. Cuando se cambia el c�digo Python, incluyendo el archivo de manifiesto, es necesario un reinicio del servidor. Cuando se cambian archivos XML o CSV es necesaria una actualizaci�n del m�dulo; incluso en caso de duda, realice ambas: actualizaci�n del m�dulo y reinicio del servidor.