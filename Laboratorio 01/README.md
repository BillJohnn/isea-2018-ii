## Observaciones y Conclusiones
 	1. Odoo es un sistema de ERP integrado de código abierto.

 	2. Odoo mantiene un sitio web en el que hace referencia a los módulos oficiales así como aquellos contribuidos por la comunidad de desarrolladores en un concepto similar a las tiendas de aplicaciones de Apple y Google. Los módulos comunitarios pueden ser referenciados de forma gratuita.

 	3. Las PYMES en la era digital, que no incorporen un ERP como Odoo en sus operaciones diarias, corren el riesgo de desaparecer en este  mundo global y competitivo.

 	4. Una de las ventajas de usar ODOO es la flexibilidad ya que se pueden añadir aplicaciones dependiendo del crecimiento de la empresa, añadiendo las aplicaciones de una en una a medida que las necesidades cambien y su clientela crezca.

 	5. Otra opcion para la intalacion de PostgressSql es SQLEctron, el cual tiene las mismas caracteristicas de PGAdmin V.4 solo que con otra interfaz.

 	6. Creamos una cuenta en Odoo con los derechos de administrador y el acceso total a todas las apps.
---