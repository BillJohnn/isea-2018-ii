## Instlaci�n en Ubuntu Server

1. Primero accdemos como usuario root con el siguiente comando:
	$ sudo su
	Imagen: 1.png	

2. Para la instalaci�n solo se necesita de 3 comandos:
	Descargamos el contenido de la p�gina web de ODOO
	$ wget -O - https://nightly.odoo.com/odoo.key | apt-key add	
	Imagen: 2.png

	Modificamos
	$ echo "deb http://nightly.odoo.com/11.0/nightly/deb/ ./" >> 
	/etc/apt/sources.list.d/odoo.list
	Imagen: 3.png

	Instalamos ODOO
	$ apt-get update && apt-get install odoo

3. Comprobamos la instalaci�n iniciando el servicio y mirando su estado.
	$ service odoo start 
	$ service odoo status 
	Imagen: 4.png
	Imagen: 5.png

4. Para poder acceder a nuestro ODOO debemos saber la direcci�n IP de nuestro Ubunru Server para eso ejecutamos "ifconfig".

5. Luego en el navegador de nuestra maquina ingresamos la direcci�n IP junto con ":8069" y nos llevara a nuestro servicio de ODOO.

	Imagen: 6.png
	Imagen: 7.png
	Imagen: 8.png

6. Detenemos nuestro servicio con el comando:
	$ service odoo stop
	Imagen: 9.png
	Imagen: 10.png
---