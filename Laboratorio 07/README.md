﻿## Laboratorio 7
## CONFIGURACIÓN BÁSICA INVENTARIOS, POS Y COMPRAS 
1. Empresa ACME con logo de Tecsup.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/5a3899911d10e13bdae41b6f49df1a0a783f1260/Laboratorio%2007/Imagenes/Logo%20tecsup%20de%20acme.gif)]

2. Creación de al menos 30 productos tecnológicos. Tres deben ser creados manualmente y el resto subido a través de un csv.

        Manualmente
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/Creacion%20de%20productos%20manualmente%201.gif)]

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/Creacion%20de%20productos%20manualmente%202.gif)]

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/Creacion%20de%20productos%20manualmente%203.gif)]

    A través de un csv

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/Importacion%20de%20productos.gif)]

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/Importacion%20de%20productos%202.gif)]

3. Realizar un ajuste de inventario para cada uno de estos productos.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/Ajuste%20de%20inventario.gif)]

4. Instalar el punto de venta y agregar un medio de pago llamado VISA.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/Punto%20de%20venta%20y%20medio%20de%20pago.gif)]

5.  Generar al menos 15 ventas en el Punto de Venta. No es necesario demostrar el funcionamiento del offline pero se tomará como punto adicional.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/15%20puntos%20de%20venta%20offline.gif)]

6. Instalar el módulo de compras y generar al menos cinco compras de tres proveedores. 
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/a90603695d65811d323e4fb4fb233f4aad728432/Laboratorio%2007/Imagenes/Cinco%20compras%20de%20tres%20proveedores.gif)]

7. Realizar la recepción parcial de dos compras, y el resto recepcionar el stock por completo.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/src/master/Laboratorio%2007/Imagenes/Recepci%C3%B3n%20parcial%20de%20dos%20compras%2C%20y%20el%20resto%20recepcionar%20el%20stock.gif)]

8. Pagar solamente a dos proveedores completamente, mientras que al tercero solamente se le pagará una compra y tendrá pendiente un saldo.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/src/master/Laboratorio%2007/Imagenes/Pagar%20solamente%20a%20dos%20proveedores%20y%20el%20tercero%20tendr%C3%A1%20pendiente%20un%20saldo.gif)]


###### Observaciones y Conclusiones
1. El modulo Punto de Venta se puede utilizar en línea o fuera de línea en iPads, tabletas Android o portátiles.
2. El Punto de Venta de Odoo está totalmente integrado con los módulos de Inventario y de Contabilidad. Cualquier transacción con su punto de venta será automáticamente registrado en la gestión de inventario y en la contabilidad e, incluso en el módulo de CRM ya que el cliente puede identificarse desde este módulo.