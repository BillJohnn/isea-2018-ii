## Laboratorio 8
## CONFIGURACI�N B�SICA � VENTAS Y CRM 
1. Empresa ACME con logo de Tecsup.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/5a3899911d10e13bdae41b6f49df1a0a783f1260/Laboratorio%2007/Imagenes/Logo%20tecsup%20de%20acme.gif)]

2. Modificaci�n de la plantilla de Pedido de Compras y generaci�n de dicho reporte.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/c865f1a47bc24986c2418048ad7fa8996bb47f6a/Laboratorio%2008/Imagenes/Modificaci%C3%B3n%20de%20la%20plantilla%20de%20Pedido%20de%20Compras%20y%20generaci%C3%B3n%20de%20dicho%20reporte.gif)]

3. Generar al menos 5 ventas en el m�dulo Ventas a tres clientes distintos.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/c865f1a47bc24986c2418048ad7fa8996bb47f6a/Laboratorio%2008/Imagenes/Generar%20al%20menos%205%20ventas%20en%20el%20m%C3%B3dulo%20Ventas%20a%20tres%20clientes%20distintos.gif)]

4. Realizar la entrega parcial de dos ventas, y el resto entregar el stock por completo.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/c865f1a47bc24986c2418048ad7fa8996bb47f6a/Laboratorio%2008/Imagenes/Realizar%20la%20entrega%20parcial%20de%20dos%20ventas%2C%20y%20el%20resto%20entregar%20el%20stock%20por%20completo.gif)]

5.  Cobrar solamente a dos clientes completamente, mientras que al tercero solamente se le cobrar� una venta y tendr� pendiente un saldo.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/c865f1a47bc24986c2418048ad7fa8996bb47f6a/Laboratorio%2008/Imagenes/Cobrar%20solamente%20a%20dos%20clientes%20completamente%2C%20mientras%20que%20al%20tercero%20solamente%20se%20le%20cobrar%C3%A1%20una%20venta%20y%20tendr%C3%A1%20pendiente%20un%20saldo..gif)]

6. Instalar el m�dulo de CRM y generar al menos tres oportunidades de venta en distintas etapas.
[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/c865f1a47bc24986c2418048ad7fa8996bb47f6a/Laboratorio%2008/Imagenes/Instalar%20el%20m%C3%B3dulo%20de%20CRM%20y%20generar%20al%20menos%20tres%20oportunidades%20de%20venta%20en%20distintas%20etapas.gif)]


###### Observaciones y Conclusiones
1. CRM (Customer Relationship Management) nos referimos a la estrategia de negocios que la empresa utiliza para conseguir la fidelizaci�n del cliente.
2. El CRM de Odoo gestiona la cartera de clientes, tanto potenciales como existentes, sencilla y eficazmente, siendo esta informaci�n accesible para todo el personal autorizado de la compa��a.
3. CRM no s�lo est� limitado a la relaci�n con los clientes, si no que se refiere a todas aquellas personas o entidades relacionadas con la organizaci�n: proveedores, socios, empleados, etc.
4. Odoo permite la integraci�n con el correo electr�nico, existiendo la posibilidad de organizar y filtrar autom�ticamente los correos entrantes enfocados hacia las oportunidades o equipos de ventas.
5. En Odoo todo est� integrado, desde la ficha de cliente podremos acceder a todo lo relacionado con la compa��a y dicho cliente: contabilidad, ventas, TPV� Y si estamos hablando de una empresa u organizaci�n, podremos comprobar los datos del personal asociado a la misma.
6. Toda la informaci�n ser� compartida por todos aquellos empleados con acceso autorizado, por lo que la modificaci�n de informaci�n en el perfil del cliente, se actualiza autom�ticamente para que el resto de empleados pueda realizar una gesti�n m�s eficaz de los mismos.