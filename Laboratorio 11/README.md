## Laboratorio 11
## VISTAS AVANZADO
1. Agregar decoradores a las vistas de Lista

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/b5aaeb6380f5f18c8a892578321d7018510ca05e/Laboratorio%2011/Capturas/Agregar%20decoradores%20a%20las%20vistas%20de%20Lista.gif)]

2. Agregar una vista calendario

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/b5aaeb6380f5f18c8a892578321d7018510ca05e/Laboratorio%2011/Capturas/Agregar%20una%20vista%20calendario.gif)]

3. Agregar una vista Kanban

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/src/master/Laboratorio%2011/Capturas/Agregar%20una%20vista%20Kanban.gif)]

4. Agregar un filtro de búsqueda

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/b5aaeb6380f5f18c8a892578321d7018510ca05e/Laboratorio%2011/Capturas/Agregar%20un%20filtro%20de%20b%C3%BAsqueda.gif)]

5. Agrega una vista Reporte

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/b5aaeb6380f5f18c8a892578321d7018510ca05e/Laboratorio%2011/Capturas/Agrega%20una%20vista%20Reporte.gif)]

###### Observaciones y Conclusiones
1. El asistente de Odoo se utiliza para proporcionar una sesión interactiva al usuario a través de formularios dinámicos. Wizard es un modelo que se creará al heredar la clase models.TransientModel en lugar de la clase Model.
2. El uso de wizard me parecio muy util ya que puede ahorrar mucho tiempo al momento de que el usuario realice opercaciones.
3. El .env nos sirve para enlazarnos a cualquier modelo de nuestro base de datos, 
4. Los modelos temporales son utilizados por los wizard para hacer operaciones.