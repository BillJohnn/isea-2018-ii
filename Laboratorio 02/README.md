## Observaciones y Conclusiones
 	1. En Odoo las tareas se realizan mediante la creación de módulos.

 	2. Los módulos personalizan el comportamiento de una instalación de Odoo, añadiendo nuevos comportamientos o modificando los existentes (incluyendo nuevas características o modificando las de otros módulos).

 	3. Todo se articula por módulos o aplicaciones, de tal manera que no es necesario instalar todas las aplicaciones, sino que cada empresa implementará sólo aquellos módulos que necesite en cada momento.

 	4. Odoo sigue una arquitectura MVC; El modelo, define la estructura de los datos; La vista, describe la interfaz con el usuario; El controlador, soporta la lógica de negocio de la aplicación.

 	5. La capa modelo en Odoo es definida por objetos Python cuyos datos son almacenados en una base de datos PostgreSQL. El mapeo de la base de datos es gestionado automáticamente por Odoo, y el mecanismo responsable por esto es el modelo objeto relacional.

 	6. La capa vista en Odoo son definidas usando XML, las cuales son usadas por un framework del cliente web para generar vistas HTML de datos.

 	7. Los modelos describen los objetos de negocio, como una oportunidad, una orden de venta, o un socio. Un modelo tiene una lista de atributos y también puede definir su negocio específico.

 	8. Los modelos son implementados usando clases Python derivadas de una plantilla de clase de Odoo. Estos son traducidos directamente a objetos de base de datos, y Odoo se encarga de esto automáticamente cuando el módulo es instalado o actualizado.
---
El archvivo Doc1.docx contiene imagenes de la instalación de Odoo, pgAdmin y la creación del modulo.
---