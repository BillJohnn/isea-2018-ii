﻿## Documentación 
###### Creación de módulo
1. Vamos a crear un módulo en Odoo para esto debemos utilizar el CMD como administrador.
![CMD Admi](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2001.PNG)

2. Nos dirigimos a la carpeta raiz de Odoo y luego a /server/odoo/addons
![picture](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2002.PNG)

3. Ingresamos el siguiente codigo para crear el modulo.
![codigo](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2003.PNG)

4. En la carpeta podemos ver el contenido del nuevo modulo.
![contenido de modulo](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2004.PNG)

###### Creación de modelos
1. Abrimos nuestro editor de codigo con los permisos de administrador para luego poder editar el archivo "__manifest__.py" donde se mostrara los datos basicos cuando se instale el modulo.
![picture](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2005.PNG)

2. Nos dirigimos a la carpeta modelos, archivo modelos.py para modificar y crear los datos de almacenamiento.
![picture](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2006.PNG)

3. Nos vamos a la carpeta views y creamos los archivos: alumnos.xml, areas.xml y cursos.xml. A qui realizamos como se vera nuestro ventana alumno.
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2007.PNG)

4. En las lineas del 5 al 14 estamos creando el formulario que va tener los campos de edad y nombre. Creamos una seccion de alumnos.
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2008.PNG)

5. Creaos un top menu cn categorias y en la seccion debajo de alumnos creamos un lisado donde llamara al formulario de alumnos.
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2009.PNG)

6. Ahora pocedemos a instalar el modulo pero esto debemos estar en modo desarrollador.
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2010.PNG)

7. Una vez que lo tenemos insalado, buscamos en la parte superior el nombre de nuestro modulo para que luego nos lleve a la siguiente interfaz.
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2011.PNG)

8. Agregamos una vista mas en nuestro manifest y hacemos los demas pasos para crear la vista de areas y cursos y luego actualizamos el modulo.
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2012.PNG)
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2013.PNG)
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2014.PNG)
![manifest](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/646bf5e8cfa56eb3e147e2e1b4dc2569897d1fbf/Laboratorio%2005/Documentaci%C3%B3n/Documentaci%C3%B3n%20-%2015.PNG)

---
Se adjunta imagenes
---