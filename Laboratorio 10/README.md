## Laboratorio 10
## CREACIÓN DE MODELOS AVANZADO 
1. Completar el ejercicio propuesto del módulo OpenAcademy

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/7b7aa7fe8b1459b310db60e576dff74945e94dfa/Laboratorio%2010/Capturas/Instalaci%C3%B3n%20de%20Odoo.gif)]

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/7b7aa7fe8b1459b310db60e576dff74945e94dfa/Laboratorio%2010/Capturas/Vista%20Open%20Academy.gif)]

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/7b7aa7fe8b1459b310db60e576dff74945e94dfa/Laboratorio%2010/Capturas/CreandoCurso.gif)]

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/7b7aa7fe8b1459b310db60e576dff74945e94dfa/Laboratorio%2010/Capturas/Sesiones.gif)]

###### Observaciones y Conclusiones
1. Para la vista de los datos de los modelos, es necesario tener un action de listado (course_list_action), en este se definen los campos (fields), entre etiquetas group, notebook, page, etc.

2. el formulario y vista de cada registro es generado automaticamente por Odoo pero también es posible crear un formulario más personalizado manualmente (course_form_view).

3. Aprendimos a heredar uno de los modelos que posee Odoo por defecto, agregamos campos a dicho modelo y una vista desde nuestro módulo creado.

4. Es necesario reiniciar el servido cuando se modifica el codigo de python. Si solo modificamos el codigo xml con una actualizacion del modulo basta.