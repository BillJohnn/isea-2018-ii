## Laboratorio 12
## INTERNACIONALIZACIÓN Y WEB

1. Agregar un grupo de permisos para el módulo OpenAcademy

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/1a42c124568805d2963af8cbc0ecdde2c7e3952a/Laboratorio%2012/Capturas/Agregar%20un%20grupo%20de%20permisos%20para%20el%20m%C3%B3dulo%20OpenAcademy.gif)]

[![Watch the video](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/1a42c124568805d2963af8cbc0ecdde2c7e3952a/Laboratorio%2012/Capturas/Agregar%20un%20grupo%20de%20permisos%20para%20el%20m%C3%B3dulo%20OpenAcademy%202.gif)]

2. Listado de Cursos en sitio web

[![img](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/1a42c124568805d2963af8cbc0ecdde2c7e3952a/Laboratorio%2012/Capturas/2.2%20Listado%20de%20Cursos%20en%20sitio%20web.PNG)]

[![img](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/1a42c124568805d2963af8cbc0ecdde2c7e3952a/Laboratorio%2012/Capturas/2.2%20Listado%20de%20Cursos%20en%20sitio%20web%202.PNG)]

3. Listado de Sesiones en sitio web 

[![img](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/1a42c124568805d2963af8cbc0ecdde2c7e3952a/Laboratorio%2012/Capturas/2.3%20Listado%20de%20Sesiones%20en%20sitio%20web.PNG)]

[![img](https://bitbucket.org/BillJohnn/isea-2018-ii/raw/1a42c124568805d2963af8cbc0ecdde2c7e3952a/Laboratorio%2012/Capturas/2.3%20Listado%20de%20Sesiones%20en%20sitio%20web%202.PNG)]


###### Observaciones y Conclusiones
1. Con los controladores podemos interactuar con nuestra pagina web, como mostrar a tus clientes los acotencimiento importantes de la empresa.

2. Muy a parte del uso del controlador para la interaccion con la pagina web es importante imlementar una vista, en esta podemos recibir los datos y renderizarlos en una vista del la pagian web.
